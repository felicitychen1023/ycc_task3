/* call createfile.so file to create a new file at specified address and write name in it */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{

  char file_address[40]="";

  strcpy(file_address,argv[1]);
 
  // call the function realized in createfile.so
  CreateFile(file_address);

  return 0;
 
}
